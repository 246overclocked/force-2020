package frc.robot;

import edu.wpi.first.hal.HAL;
import edu.wpi.first.hal.sim.DriverStationSim;
import edu.wpi.first.wpilibj.Sendable;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableRegistry;

import java.lang.reflect.Field;
import java.util.Map;

import org.junit.After;
import org.junit.Before;

/**
 * Common superclass for tests that use hardware.  This will clean up hardware allocations (DIO
 * ports, PWM channels, etc) to allow individual unit tests to be unaffected by test ordering.
 *
 * All tests for commands, subsystems, etc should subclass this.
 */
public class HardwareTestHarness {

  // Initialize hardware to be in enabled test mode
  static {
    HAL.initialize(500, 0);
    DriverStationSim dsSim = new DriverStationSim();
    dsSim.setDsAttached(true);
    dsSim.setAutonomous(false);
    dsSim.setEnabled(true);
    dsSim.setTest(true);
  }

  @Before
  @After
  @SuppressWarnings("PMD.AvoidCatchingGenericException") // AutoCloseable#close throws raw Exception
  public void closeHardwareResources() {
    SendableRegistry.foreachLiveWindow(0, callbackData -> {
      Sendable sendable = callbackData.sendable;
      if (sendable instanceof AutoCloseable) {
        try {
          ((AutoCloseable) sendable).close();
        } catch (Exception e) {
          e.printStackTrace(System.err);
        }
      }
    });
  }

  @Before
  @After
  public void removeSendables() {
    SendableRegistry.foreachLiveWindow(0, data -> SendableRegistry.remove(data.sendable));
  }

  @Before
  @After
  public void clearShuffleboard() {
    // Shuffleboard doesn't have a 'clear' function and the implementation details are private,
    // so we're forced to use reflection here to get access to and clear the tab data
    try {
      Field root = Shuffleboard.class.getDeclaredField("root");
      root.setAccessible(true);
      Field tabs = root.get(null).getClass().getDeclaredField("m_tabs");
      tabs.setAccessible(true);
      ((Map<String, ShuffleboardTab>) tabs.get(root.get(null))).clear();
    } catch (ReflectiveOperationException e) {
      e.printStackTrace(System.err);
    }
  }
}
