package frc.robot.command;

import frc.robot.CommandTestHarness;
import frc.robot.commands.DriveWithJoysticks;
import frc.robot.subsystems.Drivetrain;

import edu.wpi.first.wpilibj2.command.CommandScheduler;

import java.util.Set;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example tests for a command.
 */
public class DriveWithJoysticksCommandTest extends CommandTestHarness {
  @Test
  public void testRequires() {
    Drivetrain drivetrain = new Drivetrain();
    DriveWithJoysticks command = new DriveWithJoysticks(drivetrain, () -> 0, () -> 0);

    assertEquals(
        "Command should require the drivetrain subsystem",
        Set.of(drivetrain),
        command.getRequirements()
    );
  }

  @Test
  public void testSetsMotor() {
    double[] arcadeValues = new double[2];
    Drivetrain drivetrain = new Drivetrain() {
      @Override
      public void arcadeDrive(double speed, double rotation) {
        arcadeValues[0] = speed;
        arcadeValues[1] = rotation;
      }
    };
    DriveWithJoysticks command = new DriveWithJoysticks(drivetrain, () -> -1, () -> .5);
    CommandScheduler.getInstance().schedule(command);
    CommandScheduler.getInstance().run();
    assertEquals(-1, arcadeValues[0], 0);
    assertEquals(.5, arcadeValues[1], 0);
  }
}
