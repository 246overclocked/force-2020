package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;


import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import static frc.robot.Constants.*;

public class Climb extends SubsystemBase{

    //Defining SparkMax for climb
    private CANSparkMax m_leadMotor = new CANSparkMax(SPARK_MAX_LEAD_PORT, MotorType.kBrushless);
    private CANSparkMax m_followMotor = new CANSparkMax(SPARK_MAX_FOLLOW_PORT, MotorType.kBrushless);

    public Climb(){
        m_leadMotor.restoreFactoryDefaults();
        m_followMotor.restoreFactoryDefaults();
        m_followMotor.follow(m_leadMotor);
    }
    public void setClimbControlled(double speed) {
        m_leadMotor.set(speed);
    }
    
}