/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import static frc.robot.Constants.*;

public class Shooter extends SubsystemBase {
  private final DoubleSolenoid piston =
          new DoubleSolenoid(SHOOTER_PCM_PORT_FORWARD, SHOOTER_PCM_PORT_REVERSE);
  private final WPI_TalonSRX shooterMotorMaster = new WPI_TalonSRX(SHOOTER_MASTER_PORT);
  private final WPI_TalonSRX shooterMotorSlave = new WPI_TalonSRX(SHOOTER_SLAVE_PORT);

  public Shooter() {
    shooterMotorSlave.follow(shooterMotorMaster);
  }

  public void setPiston(Value position) {
    piston.set(position);
  }

  public void setShooterRaw(double speed) {
    shooterMotorMaster.set(ControlMode.PercentOutput, speed);
  }
}
