/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;
import frc.robot.commands.ActuateClimbMotor;
import frc.robot.commands.ActuateHopperMotor;
import frc.robot.commands.ActuateIntakeMotor;
import frc.robot.commands.ActuateIntakePistons;
import frc.robot.commands.ActuateShooterMotor;
import frc.robot.commands.ActuateShooterPiston;
import frc.robot.commands.ActuateTowerMotor;
import frc.robot.commands.ActuateTurretMotorRaw;
import frc.robot.commands.DriveStraight;
import frc.robot.commands.DriveWithJoysticks;
import frc.robot.olibs.util.DualAction;
import frc.robot.olibs.util.DualActionController;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Hopper;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.Tower;
import frc.robot.subsystems.Turret;
import frc.robot.subsystems.Climb;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.Command;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...

  private final Intake intake = new Intake();
  private final Drivetrain drivetrain = new Drivetrain();
  private final Hopper hopper = new Hopper();
  private final Tower tower = new Tower();
  private final Turret turret = new Turret();
  private final Shooter shooter = new Shooter();
  private final Climb climb = new Climb();
  

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Configure the button bindings
    configureButtonBindings();
    setupDashboard();
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    final FluentHID driver = new FluentHID(new DualActionController(0));
    final FluentHID operator = new FluentHID(new DualActionController(1));
    driver.button(DualAction.LEFT_TRIGGER)
            .whileHeld(new ActuateIntakeMotor(intake, 1));
    driver.button(DualAction.RIGHT_TRIGGER)
            .whileHeld(new ActuateIntakeMotor(intake, -1));
    driver.button(DualAction.LEFT_BUMPER)
            .whenPressed(new ActuateIntakePistons(intake, Value.kForward));
    driver.button(DualAction.RIGHT_BUMPER)
            .whenPressed(new ActuateIntakePistons(intake, Value.kReverse));
    
    drivetrain.setDefaultCommand(new DriveWithJoysticks(
        drivetrain, 
        () -> driver.getRawAxis(DualAction.LEFT_Y_AXIS), 
        () -> driver.getRawAxis(DualAction.RIGHT_X_AXIS)
      )
    );

    
    operator.button(DualAction.A).whileHeld(new ActuateHopperMotor(hopper, 1));
    operator.button(DualAction.B).whileHeld(new ActuateHopperMotor(hopper, -1));
    operator.button(DualAction.RIGHT_BUMPER).whileHeld(new ActuateShooterMotor(shooter, 0.8));
    operator.button(DualAction.RIGHT_TRIGGER).whileHeld(new ActuateTowerMotor(tower, 0.5));
    operator.button(DualAction.LEFT_TRIGGER).whileHeld(new ActuateTowerMotor(tower, -0.5));
    operator.button(DualAction.BACK_BUTTON).whileHeld(new ActuateTurretMotorRaw(turret, 0.1));
    operator.button(DualAction.START_BUTTON).whileHeld(new ActuateTurretMotorRaw(turret, -0.1));
    /*
    operator.button(DualAction.Y).toggleWhenPressed(new ResolveTarget(
      turret,
      () -> turret.getCurrentHeading(),
      () -> NetworkTableInstance.getDefault().
      getTable("limelight").getEntry("tx").getDouble(0)));
    */
    operator.button(DualAction.X).whenPressed(new ActuateShooterPiston(shooter, Value.kForward));
    operator.button(DualAction.RIGHT_TRIGGER)
            .whenPressed(new ActuateShooterPiston(shooter, Value.kReverse));
  }

  private void setupDashboard() {
    ShuffleboardTab debugTab = Shuffleboard.getTab("debug");
    debugTab.add("Hood Shot", new ActuateShooterMotor(shooter, 1));
    debugTab.add("Hood Forward", new ActuateShooterPiston(shooter, Value.kForward));
    debugTab.add("Hood Reverse", new ActuateShooterPiston(shooter, Value.kReverse));
    debugTab.add("Intake Forward", new ActuateIntakePistons(intake, Value.kForward));
    debugTab.add("Intake Reverse", new ActuateIntakePistons(intake, Value.kReverse));
  }

  public Command getAutonCommand() {
    return (new DriveStraight(drivetrain)).withTimeout(0.3);
  }
}
