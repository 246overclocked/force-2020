
package frc.robot.commands;

import frc.robot.subsystems.Tower;

import edu.wpi.first.wpilibj2.command.CommandBase;

public class ActuateTowerMotor extends CommandBase {
  private final Tower tower;
  private final double speed;

  public ActuateTowerMotor(Tower tower, double speed) {
    this.tower = tower;
    this.speed = speed;
    addRequirements(tower);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    tower.setTowerRaw(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    tower.setTowerRaw(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
